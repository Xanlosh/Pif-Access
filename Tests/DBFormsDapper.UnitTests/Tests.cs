﻿using System;
using System.Diagnostics.Contracts;
using DBFormsDapper.Controllers;
using DBFormsDapper.Core;
using NUnit.Framework;

namespace DBFormsDapper.UnitTests
{
    [TestFixture]
    public class Tests
    {     
        [Test]
        public void VacationReportTest()
        {     
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateVacationReport();
        }
        [Test]
        public void EmployeeReportTest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateEmployeeReport();
        }
        [Test]
        public void OrdersReportTest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateOrdersReport();
        }
        [Test]
        public void CirculumReportTest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateCirculumReport();
        }
        [Test]
        public void VacationDayReportTest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateVacationDayReport();
        }
        [Test]
        public void ClientReportTest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateClientReport();
        }
        [Test]
        public void PrintVacationRequest()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateVacationRequestReport(1);
        }
        [Test]
        public void PrintOrderReport()
        {
            Connection dbConnection = new Connection();
            ReportGenerator.GenerateOrderReport();
        }
    }
}