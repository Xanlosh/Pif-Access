﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Dapper;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Models;
using iTextSharp.testutils;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DBFormsDapper.Core
{
    public static class ReportGenerator
    {
        private static readonly Font FontTr = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
        private static readonly Font FontH1 = FontFactory.GetFont("HELVETICA", 8, BaseColor.WHITE);
        private static readonly Font FontH2 = FontFactory.GetFont("HELVETICA", 8, BaseColor.WHITE);

        private static Document ReadyDocument(string path)
        {
            Document doc = new Document();
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            writer.PageEvent = new PdfPageEvents();
            doc.SetPageSize(PageSize.A3.Rotate());
            doc.Open();
            return doc;
        }

        private static string CreateApplicationFolder()
        {
            string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                "Reports");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        public static void GenerateEmployeeReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "EmployeeReport.pdf"));

            #region Gathering Data

            List<Department> departmentlist = (List<Department>) Connection.DbConnection.GetAll<Department>();
            List<Employee> employees = Connection.DbConnection
                .Query<Employee>(
                    "SELECT * FROM ((tblEmployee INNER JOIN tblWork ON tblEmployee.idEmployee = tblWork.fiEmployee) INNER JOIN tblDepartment ON tblWork.fiDepartment = tblDepartment.idDepartment);")
                .ToList();

            #endregion

            PdfPTable table = new PdfPTable(13);

            table.AddCell(new PdfPCell(new Phrase("id", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("First Name", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Last Name", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Birtdate", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Matriculation", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Phonenumber", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Personal Phonenumber", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Email", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Personal Email", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("City", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("ZipCode", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Streetname", FontH1)) {BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Streetnumber", FontH1)) {BackgroundColor = BaseColor.BLUE});
            foreach (var department in departmentlist)
            {
                PdfPCell cell = new PdfPCell(new Phrase(department.dtTitle)) {Colspan = 13, Border = 0, Padding = 10};
                table.AddCell(cell);
                foreach (Employee employee in employees.Where(l => l.getDepartment() == department.dtTitle).ToList())
                {
                    table.AddCell(new Phrase(employee.idEmployee.ToString(), FontTr));
                    table.AddCell(new Phrase(employee.dtFirstname, FontTr));
                    table.AddCell(new Phrase(employee.dtLastname, FontTr));
                    table.AddCell(new Phrase(employee.dtBirthdate.ToLongDateString(), FontTr));
                    table.AddCell(new Phrase(employee.dtMatriculation, FontTr));
                    table.AddCell(new Phrase(employee.dtPhonenumber, FontTr));
                    table.AddCell(new Phrase(employee.dtPrivatephonenumber, FontTr));
                    table.AddCell(new Phrase(employee.dtPrivateemail, FontTr));
                    table.AddCell(new Phrase(employee.dtEmail, FontTr));
                    table.AddCell(new Phrase(employee.dtCity, FontTr));
                    table.AddCell(new Phrase(employee.dtZipcode, FontTr));
                    table.AddCell(new Phrase(employee.dtStreetname, FontTr));
                    table.AddCell(new Phrase(employee.dtStreetnumber, FontTr));
                }
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateVacationReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "VacationReport.pdf"));

            #region Gathering Data

            List<Department> departmentlist = (List<Department>) Connection.DbConnection.GetAll<Department>();
            List<VacationRequest> vacationRequests = Connection.DbConnection.GetAll<VacationRequest>().ToList();
            List<Employee> claimants = vacationRequests.Select(l => l.getClaimant()).ToList();

            #endregion

            PdfPTable table = new PdfPTable(7);

            foreach (var department in departmentlist)
            {
                PdfPCell depcell = new PdfPCell(new Phrase(department.dtTitle, FontH1)) {Colspan = 7,BackgroundColor = BaseColor.BLUE};
                table.AddCell(depcell);
                foreach (var claimant in claimants.GroupBy(i => i.idEmployee).Select(g => g.First())
                    .Where(i => i.getDepartment() == department.dtTitle).ToList())
                {
                    PdfPCell empcell =
                        new PdfPCell(new Phrase(claimant.dtFirstname + " " + claimant.dtLastname,
                            FontH2)) {Colspan = 7,BackgroundColor = BaseColor.RED};
                    table.AddCell(empcell);
                    foreach (var vacationRequest in vacationRequests.Where(l => l.fiClaimant == claimant.idEmployee)
                        .ToList())
                    {
                        table.AddCell(new Phrase(vacationRequest.idVacation.ToString(), FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtRequestDate.ToLongDateString(),
                            FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtStartDate.ToLongDateString(),
                            FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtEndDate.ToLongDateString(),
                            FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtIsApproved.ToString(),
                            FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtRevisingDate.ToLongDateString(),
                            FontTr));
                        table.AddCell(new Phrase(vacationRequest.dtComment.ToString(CultureInfo.InvariantCulture),
                            FontTr));
                    }
                }
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateOrdersReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "OrdersReport.pdf"));

            #region Gathering Data
    
            List<Order> orders = (List<Order>) Connection.DbConnection.GetAll<Order>();

            #endregion

            PdfPTable table = new PdfPTable(8);


            table.AddCell(new PdfPCell(new Phrase("id",FontH1)){BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("dtOrderDate",FontH1)){BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("isPaid",FontH1)){BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new Phrase("Client Name",FontH1)){BackgroundColor = BaseColor.BLUE});
            table.AddCell(new PdfPCell(new PdfPCell(new Phrase("Address",FontH1)) {Colspan = 4 , BackgroundColor = BaseColor.BLUE}));

            
            foreach (var order in orders)
            {
                table.AddCell(new Phrase(order.idOrder.ToString(), FontTr));
                table.AddCell(new Phrase(order.dtOrderDate.ToLongDateString(), FontTr));
                table.AddCell(new Phrase(order.dtisPaid.ToString(CultureInfo.InvariantCulture), FontTr));
                table.AddCell(new Phrase(order.getClient().dtFirstname + " " + order.getClient().dtLastname, FontTr));
                table.AddCell(new Phrase(order.getAddress().dtCity, FontTr));
                table.AddCell(new Phrase(order.getAddress().dtStreetname, FontTr));
                table.AddCell(new Phrase(order.getAddress().dtStreetnumber, FontTr));
                table.AddCell(new Phrase(order.getAddress().GetCountry().dtCountryName, FontTr));
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateCirculumReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "CirculumReport.pdf"));

            #region Gathering Data

            List<DepartmentHistroy> history = Connection.DbConnection.GetAll<DepartmentHistroy>().ToList();

            #endregion
            
            PdfPTable table = new PdfPTable(2);


            foreach (var item in history.GroupBy(i => i.fiEmployee).Select(g => g.First()).ToList())
            {
                table.AddCell(
                    new PdfPCell(new Phrase(item.GetEmployee().dtFirstname + " " + item.GetEmployee().dtLastname,FontH1))
                    {
                        Colspan = 2,
                        BackgroundColor = BaseColor.BLUE
                    });
                foreach (var department in history.Where(i => i.fiEmployee == item.GetEmployee().idEmployee).ToList())
                {
                    table.AddCell(new Phrase(department.dtJoinDate.ToLongDateString(),FontTr));
                    table.AddCell(new Phrase(department.GetDepartment().dtTitle,FontTr));
                }
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateVacationDayReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "VacationDayReport.pdf"));

            #region Gathering Data
            const int totalHoliday = 30;
            int remainingHoliday = totalHoliday;
            List<VacationRequest> vacationRequests = Connection.DbConnection.GetAll<VacationRequest>()
                .Where(l => l.dtStartDate.Year == DateTime.Now.Year).Where(l => l.dtIsApproved).ToList();
            
            #endregion
            
            PdfPTable table = new PdfPTable(4);
            foreach (var vacationRequestByEmployee in vacationRequests.GroupBy(l => l.fiClaimant).Select(g => g.First())
                .ToList())
            {
                table.AddCell(new PdfPCell(new Phrase(vacationRequestByEmployee.getClaimant().dtFirstname +
                                                      vacationRequestByEmployee.getClaimant()
                                                          .dtLastname)) {Colspan = 4});
                foreach (var vacationRequest in vacationRequests.Where(l =>
                    l.fiClaimant == vacationRequestByEmployee.getClaimant().idEmployee))
                {
                    table.AddCell(new Phrase(vacationRequest.idVacation));
                    table.AddCell(new Phrase(vacationRequest.dtStartDate.Date.ToLongDateString()));
                    table.AddCell(new Phrase(vacationRequest.dtEndDate.Date.ToLongDateString()));
                    table.AddCell(new Phrase(vacationRequest.dtComment));
                    remainingHoliday = remainingHoliday -
                                       (int) (vacationRequest.dtEndDate - vacationRequest.dtStartDate).TotalDays;
                }

                table.AddCell(new PdfPCell(new Phrase("Remaining Days : " + remainingHoliday)) {Colspan = 4});
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateClientReport()
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "ClientReport.pdf"));
            PdfPTable table = new PdfPTable(8);

            #region Gathering Data

            List<Client> clients = Connection.DbConnection.GetAll<Client>().ToList();

            #endregion
            table.AddCell(new Phrase("id"));
            table.AddCell(new Phrase("First Name"));
            table.AddCell(new Phrase("Last Name"));
            table.AddCell(new Phrase("Birthdate"));
            table.AddCell(new Phrase("Username"));
            table.AddCell(new Phrase("Email"));
            table.AddCell(new Phrase("Registration Date"));
            table.AddCell(new Phrase("Price Scaling"));
            foreach (var client in clients)
            {
                table.AddCell(new PdfPCell(new Phrase(client.idClient.ToString(), FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtFirstname, FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtLastname, FontTr)));
                table.AddCell(
                    new PdfPCell(new Phrase(client.dtBirthdate.ToLongDateString(), FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtUsername, FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtEmail, FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtRegistrationdate.ToLongDateString(),
                    FontTr)));
                table.AddCell(new PdfPCell(new Phrase(client.dtPricescaling.ToString(CultureInfo.InvariantCulture),
                    FontTr)));
            }

            doc.Add(table);
            doc.Close();
        }

        public static void GenerateVacationRequestReport(int id)
        {
            Document doc = ReadyDocument(Path.Combine(CreateApplicationFolder(), "VacationRequestReport_"+id +".pdf"));

            #region Gathering Data

            VacationRequest request = Connection.DbConnection.Get<VacationRequest>(id);

            #endregion
            
            PdfPTable table = new PdfPTable(2);
            table.AddCell(new Phrase("id"));
            table.AddCell(new Phrase(request.idVacation.ToString()));
            table.AddCell(new Phrase("Request Date"));
            table.AddCell(new Phrase(request.dtRequestDate.ToLongDateString()));
            table.AddCell(new Phrase("Start Date"));
            table.AddCell(new Phrase(request.dtStartDate.ToLongDateString()));
            table.AddCell(new Phrase("End Date"));
            table.AddCell(new Phrase(request.dtEndDate.ToLongDateString()));
            table.AddCell(new Phrase("Is approved"));
            table.AddCell(new Phrase(request.dtIsApproved.ToString()));
            table.AddCell(new Phrase("Revison Date"));
            table.AddCell(new Phrase(request.dtRevisingDate.ToLongDateString()));
            table.AddCell(new Phrase("Claimant"));
            table.AddCell(new Phrase(request.getClaimant().dtFirstname + " " + request.getClaimant().dtLastname));
            table.AddCell(new Phrase("Comment"));
            table.AddCell(new Phrase(request.dtComment));
            doc.Add(table);
            doc.Close();
        }

        public static void GenerateOrderReport()
        {
            string sql =
                "SELECT idOrder,dtOrderdate,dtAmount,dtProductTitle,dtCity,dtStreetname,dtStreetnumber FROM tblProduct INNER JOIN ( ( tblAddress INNER JOIN ( tblClient INNER JOIN tblOrder ON tblClient.idClient = tblOrder.fiClient ) ON tblAddress.idAddress = tblOrder.fiAddress ) INNER JOIN tblAdd ON tblOrder.idOrder = tblAdd.fiOrder ) ON tblProduct.idProduct = tblAdd.fiProduct;";
            var tests = Connection.DbConnection.Query(sql).AsList();
            string path = CreateApplicationFolder();
            path = Path.Combine(path, "OrderReport.pdf");
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document();
            doc.SetPageSize(PageSize.A4.Rotate());
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            PdfPTable table = new PdfPTable(2);
            foreach (var test in tests)
            {
                foreach (var item in test)
                {
                    table.AddCell(item.Key.ToString());
                    table.AddCell(item.Value.ToString());
                }
            }

            doc.Add(table);
            doc.Close();
        }
    }
}