﻿using System;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace DBFormsDapper.Core
{
    public class PdfPageEvents : PdfPageEventHelper
    {
        public Image ImageHeader {get; set;}
        public override void OnEndPage(PdfWriter writer, Document document)
        
        {
            // cell height 
            float cellHeight = document.TopMargin;
            Rectangle page = document.PageSize;

            PdfPTable head = new PdfPTable(2);
            head.TotalWidth = page.Width;

            PdfPCell c = new PdfPCell(ImageHeader, true);
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.FixedHeight = cellHeight;
            c.Border = PdfPCell.NO_BORDER;
            head.AddCell(c);

            c = new PdfPCell(new Phrase(
                "T3IFTECH",
                new Font(Font.FontFamily.HELVETICA, 16)
            ))
            {
                Border = PdfPCell.NO_BORDER,
                Padding = 10,
                VerticalAlignment = Element.ALIGN_BOTTOM,
                FixedHeight = cellHeight
            };
            head.AddCell(c);

            head.WriteSelectedRows(
                0, -1, // first/last row; -1 flags all write all rows
                0, // left offset
                // ** bottom** yPos of the table
                page.Height - cellHeight + head.TotalHeight,
                writer.DirectContent
            );
            int pageN = writer.PageNumber;
            String text = "Page " + pageN.ToString() + " of ";
            Paragraph footer = new Paragraph(DateTime.Now.ToLongDateString(),
                FontFactory.GetFont(FontFactory.TIMES, 10, iTextSharp.text.Font.NORMAL));
            Paragraph pageNumber = new Paragraph(pageN);
            footer.Alignment = Element.ALIGN_RIGHT;
            PdfPTable footerTbl = new PdfPTable(2);
            footerTbl.TotalWidth = 300;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell cell = new PdfPCell(footer);
            PdfPCell cell2 = new PdfPCell(pageNumber);
            cell.Border = 0;
            cell.PaddingLeft = 10;
            footerTbl.AddCell(cell);
            footerTbl.AddCell(cell2);
            footerTbl.WriteSelectedRows(0, -1, 415, 30, writer.DirectContent);
        }
    }
}