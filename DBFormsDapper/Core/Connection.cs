﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows;
using System.Configuration;
using System.IO;
using DBFormsDapper.Views;
using Microsoft.Win32;

namespace DBFormsDapper.Core
{
    public class Connection
    {
        public static IDbConnection DbConnection;
        private string _databasePath;

        public Connection()
        {
            if (File.Exists(ConfigurationManager.AppSettings["DbPath"]))
            {
                _databasePath = ConfigurationManager.AppSettings["DbPath"];
            }
            else
            {
                OpenFileDialog fileDialog = new OpenFileDialog {Filter = "Database (*.accdb)|*.accdb;"};
                if (fileDialog.ShowDialog() == true)
                {
                    _databasePath = fileDialog.FileName;
                    ConfigurationManager.AppSettings["DbPath"] = _databasePath;
                }
                else
                {
                    Environment.Exit(1);
                }
            }
            DbConnection = new OleDbConnection
            {
                ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + _databasePath + ";Persist Security Info=False;"
            };
            try
            {
                DbConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }
    }
}