﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DBFormsDapper.Controllers;

namespace DBFormsDapper.Views
{
    /// <summary>
    /// Interaction logic for AddToDepartmentWindow.xaml
    /// </summary>
    public partial class AddToDepartmentWindow : Window
    {
        private readonly EmployeeController _employeeController;
        private readonly DepartmentController _departmentController;

        public AddToDepartmentWindow()
        {
            _employeeController = new EmployeeController();
            _departmentController = new DepartmentController();
            InitializeComponent();
        }

        private void AddToDepartmentWindow_OnInitialized(object sender, EventArgs e)
        {
            ComEmployee.ItemsSource = _employeeController.EmployeeList.OrderBy(l => l.FullName())
                .Select(l => new KeyValuePair<int, string>(l.idEmployee, l.FullName())).ToList();
            ComDepartment.ItemsSource = _departmentController.DepartmentList.OrderBy(l => l.dtTitle)
                .Select(l => new KeyValuePair<int, string>(l.idDepartment, l.dtTitle)).ToList();
        }
    }
}
