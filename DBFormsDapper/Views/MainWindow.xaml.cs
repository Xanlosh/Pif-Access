﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Windows;
using System.Windows.Controls;
using DBFormsDapper.Controllers;
using DBFormsDapper.Core;
using DBFormsDapper.Models;
using MaterialDesignThemes.Wpf;

namespace DBFormsDapper.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly ClientController _clientController;
        private readonly AddressController _addressController;
        private readonly EmployeeController _employeeController;
        private readonly CountryController _countryController;
        
        public MainWindow()
        {
            var unused = new Connection();
            _clientController = new ClientController();
            _addressController = new AddressController();
            _employeeController = new EmployeeController();
            _countryController = new CountryController();
            InitializeComponent();
        }

        private void MainWindow_OnInitialized(object sender, EventArgs e)
        {
            AddressGrid.ItemsSource = _addressController.AdressList;
            Clientgrid.ItemsSource = _clientController.ClientList;
            EmployeeGrid.ItemsSource = _employeeController.EmployeeList;
            ComAddressClient.ItemsSource = _clientController.ClientList.OrderBy(l => l.FullName()).Select(l => new KeyValuePair<int,string> (l.idClient,l.FullName())).ToList();
            ComAddressCountry.ItemsSource = _countryController.CountryList.Select(l => new KeyValuePair<int,string> (l.idCountry,l.dtCountryName)).ToList();
        }
        #region AddressManagement

        #region UI Methods

        private Address RetrieveAddress()
        {
            Address address = new Address
            {
                dtCity = TbAddressCity.Text,
                dtZipcode = TbAddressZipCode.Text,
                dtStreetname = TbAddressStreetName.Text,
                dtStreetnumber = TbAddressStreetNumber.Text,
                dtisBillingaddress = CbAddressBillingAddress.IsChecked,
                fiCountry =  (int) ComAddressCountry.SelectedValue,
                fiClient = (int) ComAddressClient.SelectedValue
            };
            if (LblAddressId.Content != null)
            {
                address.idAddress = Int32.Parse(LblAddressId.Content.ToString());
            }

            return address;
        }

        private void AddressDisableButtons()
        {
            LblAddressId.Content = null;
            TbAddressCity.Text = "";
            TbAddressZipCode.Text = "";
            TbAddressStreetName.Text = "";
            TbAddressStreetNumber.Text = "";
            ComAddressCountry.SelectedValue = 0;
            ComAddressClient.SelectedValue = 0;
            Clientgrid.SelectedItem = null;
            BtnDeleteAddress.IsEnabled = false;
            BtnUpdateAddress.IsEnabled = false;
            BtnAddAddress.Content = "Add";
        }
        
        private void AddressEnableButtons()
        {
            BtnDeleteAddress.IsEnabled = true;
            BtnUpdateAddress.IsEnabled = true;
            BtnAddAddress.Content = "New Address";
        }
        private void RefreshAddressGrid()
        {
            _addressController.Refresh();
            AddressGrid.ItemsSource = null;
            AddressGrid.ItemsSource = _addressController.AdressList;
        }

        #endregion

        #region Buttons

        private void BtnUpdateAddress_OnClick(object sender, RoutedEventArgs e)
        {
            var address = RetrieveAddress();
            address.Update();
            RefreshAddressGrid();
        }

        private void BtnDeleteAddress_OnClick(object sender, RoutedEventArgs e)
        {
            var address = RetrieveAddress();
            address.Delete();
            RefreshAddressGrid();
        }

        private void BtnAddAddress_OnClick(object sender, RoutedEventArgs e)
        {
            if (LblAddressId.Content != null)
            {
                AddressDisableButtons();
                return;
            }               
            var address = RetrieveAddress();
            address.Add();
            RefreshAddressGrid();
        }

        #region Grid

        private void AddressGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Address address = (Address) AddressGrid.SelectedItem;
            if (address != null)
            {
                LblAddressId.Content = address.idAddress;
                TbAddressCity.Text = address.dtCity;
                TbAddressZipCode.Text = address.dtZipcode;
                TbAddressStreetName.Text = address.dtStreetname;
                TbAddressStreetNumber.Text = address.dtStreetnumber;
                ComAddressCountry.SelectedValue = address.fiCountry;
                ComAddressClient.SelectedValue = address.fiClient;
                
                AddressEnableButtons();
            }
        }

        #endregion
        
        #endregion
        
        #endregion
        
        #region ClientManagement

        #region UI Methods

        private void ClientDisableButtons()
        {
            Clientgrid.SelectedItem = null;
            Lblid.Content = null;
            TbFirstName.Text = "";
            TbLastName.Text = "";
            DpBirthDate.Text = "";
            TbUserName.Text = "";
            TbEmail.Text = "";
            DpRegistrationDate.Text = "";
            TbPassword.Text = "";
            TbPriceScaling.Text = "";
            BtnAddClient.Content = "Add";
            BtnDeleteClient.IsEnabled = false;
            BtnUpdateClient.IsEnabled = false;
            BtnShowClientAddresses.IsEnabled = false;
            BtnShowClientOrders.IsEnabled = false;
        }

        private void ClientEnableButtons()
        {
            BtnDeleteClient.IsEnabled = true;
            BtnUpdateClient.IsEnabled = true;
            BtnShowClientAddresses.IsEnabled = true;
            BtnShowClientOrders.IsEnabled = true;
            BtnAddClient.Content = "New Client";
        }

        private Client RetrieveClient()
        {
            try
            {
                MailAddress unused = new MailAddress(TbEmail.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }

            if (DpBirthDate.SelectedDate != null)
            {
                if (DpRegistrationDate.SelectedDate != null)
                {
                    Client client = new Client
                    {
                        dtFirstname = TbFirstName.Text,
                        dtLastname = TbLastName.Text,
                        dtBirthdate = DpBirthDate.SelectedDate.Value,
                        dtUsername = TbUserName.Text,
                        dtEmail = TbEmail.Text,
                        dtRegistrationdate = DpRegistrationDate.SelectedDate.Value,
                        dtPassword = TbPassword.Text,
                        dtPricescaling = Int32.Parse(TbPriceScaling.Text)
                    };
                    if (Lblid.Content != null)
                    {
                        client.idClient = Int32.Parse(Lblid.Content.ToString());
                    }

                    return client;
                }
            }

            return new Client();
        }

        private void RefreshClientGrid()
        {
            _clientController.Refresh();
            Clientgrid.ItemsSource = null;
            Clientgrid.ItemsSource = _clientController.ClientList;
        }

        #endregion

        #region Buttons

        private void BtnUpdateClient_OnClick(object sender, RoutedEventArgs e)
        {
            Client client = RetrieveClient();
            client.Update();
            RefreshClientGrid();
        }

        private void BtnDeleteClient_OnClick(object sender, RoutedEventArgs e)
        {
            Client client = RetrieveClient();
            client.Delete();
            RefreshClientGrid();
        }

        private void BtnShowClientAddresses_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnShowClientOrders_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnAddClient_OnClick(object sender, RoutedEventArgs e)
        {
            if (Lblid.Content != null)
            {
                ClientDisableButtons();
                return;
            }

            Client client = RetrieveClient();
            client.Add();
            RefreshClientGrid();
        }

        #endregion

        #region Grid

        private void Clientgrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Client client = (Client) Clientgrid.SelectedItem;
            if (client != null)
            {
                TbFirstName.Text = client.dtFirstname;
                TbLastName.Text = client.dtLastname;
                DpBirthDate.SelectedDate = client.dtBirthdate;
                TbUserName.Text = client.dtUsername;
                TbEmail.Text = client.dtEmail;
                DpRegistrationDate.SelectedDate = client.dtRegistrationdate;
                TbPassword.Text = client.dtPassword;
                TbPriceScaling.Text = client.dtPricescaling.ToString(CultureInfo.InvariantCulture);
                Lblid.Content = client.idClient;

                ClientEnableButtons();
            }
        }

        #endregion

        #endregion

        private void BtnAddEmployeeToDepartment_OnClick(object sender, RoutedEventArgs e)
        {
            var addToDepartmentWindow = new AddToDepartmentWindow();
            addToDepartmentWindow.Show();
        }
    }
}