﻿#pragma checksum "..\..\..\Views\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1412209A2273405CFDC4DE6FC97E912DA60762BE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Dragablz;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DBFormsDapper.Views {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 52 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbFirstName;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbLastName;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker DpBirthDate;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbUserName;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmail;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker DpRegistrationDate;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbPassword;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbPriceScaling;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnUpdateClient;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDeleteClient;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnAddClient;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnShowClientAddresses;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnShowClientOrders;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lblid;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid Clientgrid;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbAddressCity;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbAddressZipCode;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbAddressStreetName;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbAddressStreetNumber;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CbAddressBillingAddress;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComAddressCountry;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComAddressClient;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnUpdateAddress;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDeleteAddress;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnAddAddress;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblAddressId;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid AddressGrid;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeFirstName;
        
        #line default
        #line hidden
        
        
        #line 216 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeLastName;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker DpEmployeeBirthDate;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeMatriculation;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeePhonenumber;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker TbEmployeePersonalPhoneNumber;
        
        #line default
        #line hidden
        
        
        #line 230 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeePersonalEmail;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeEmail;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeCity;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeZipCode;
        
        #line default
        #line hidden
        
        
        #line 241 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeStreetName;
        
        #line default
        #line hidden
        
        
        #line 242 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmployeeStreetNumber;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComEmployeeCountry;
        
        #line default
        #line hidden
        
        
        #line 246 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnUpdateEmployee;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDeleteEmployee;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnAddEmployee;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnAddEmployeeToDepartment;
        
        #line default
        #line hidden
        
        
        #line 255 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEmployeeid;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid EmployeeGrid;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DBFormsDapper;component/views/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 15 "..\..\..\Views\MainWindow.xaml"
            ((DBFormsDapper.Views.MainWindow)(target)).Initialized += new System.EventHandler(this.MainWindow_OnInitialized);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TbFirstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.TbLastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.DpBirthDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 5:
            this.TbUserName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.TbEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.DpRegistrationDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 8:
            this.TbPassword = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TbPriceScaling = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.BtnUpdateClient = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\..\Views\MainWindow.xaml"
            this.BtnUpdateClient.Click += new System.Windows.RoutedEventHandler(this.BtnUpdateClient_OnClick);
            
            #line default
            #line hidden
            return;
            case 11:
            this.BtnDeleteClient = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\..\Views\MainWindow.xaml"
            this.BtnDeleteClient.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteClient_OnClick);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BtnAddClient = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\..\Views\MainWindow.xaml"
            this.BtnAddClient.Click += new System.Windows.RoutedEventHandler(this.BtnAddClient_OnClick);
            
            #line default
            #line hidden
            return;
            case 13:
            this.BtnShowClientAddresses = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\Views\MainWindow.xaml"
            this.BtnShowClientAddresses.Click += new System.Windows.RoutedEventHandler(this.BtnShowClientAddresses_OnClick);
            
            #line default
            #line hidden
            return;
            case 14:
            this.BtnShowClientOrders = ((System.Windows.Controls.Button)(target));
            
            #line 81 "..\..\..\Views\MainWindow.xaml"
            this.BtnShowClientOrders.Click += new System.Windows.RoutedEventHandler(this.BtnShowClientOrders_OnClick);
            
            #line default
            #line hidden
            return;
            case 15:
            this.Lblid = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.Clientgrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 93 "..\..\..\Views\MainWindow.xaml"
            this.Clientgrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Clientgrid_OnSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.TbAddressCity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.TbAddressZipCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.TbAddressStreetName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.TbAddressStreetNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.CbAddressBillingAddress = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 22:
            this.ComAddressCountry = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 23:
            this.ComAddressClient = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 24:
            this.BtnUpdateAddress = ((System.Windows.Controls.Button)(target));
            
            #line 159 "..\..\..\Views\MainWindow.xaml"
            this.BtnUpdateAddress.Click += new System.Windows.RoutedEventHandler(this.BtnUpdateAddress_OnClick);
            
            #line default
            #line hidden
            return;
            case 25:
            this.BtnDeleteAddress = ((System.Windows.Controls.Button)(target));
            
            #line 161 "..\..\..\Views\MainWindow.xaml"
            this.BtnDeleteAddress.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteAddress_OnClick);
            
            #line default
            #line hidden
            return;
            case 26:
            this.BtnAddAddress = ((System.Windows.Controls.Button)(target));
            
            #line 163 "..\..\..\Views\MainWindow.xaml"
            this.BtnAddAddress.Click += new System.Windows.RoutedEventHandler(this.BtnAddAddress_OnClick);
            
            #line default
            #line hidden
            return;
            case 27:
            this.LblAddressId = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.AddressGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 175 "..\..\..\Views\MainWindow.xaml"
            this.AddressGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AddressGrid_OnSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.TbEmployeeFirstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.TbEmployeeLastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.DpEmployeeBirthDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 32:
            this.TbEmployeeMatriculation = ((System.Windows.Controls.TextBox)(target));
            return;
            case 33:
            this.TbEmployeePhonenumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.TbEmployeePersonalPhoneNumber = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 35:
            this.TbEmployeePersonalEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            this.TbEmployeeEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 37:
            this.TbEmployeeCity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 38:
            this.TbEmployeeZipCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 39:
            this.TbEmployeeStreetName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 40:
            this.TbEmployeeStreetNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 41:
            this.ComEmployeeCountry = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 42:
            this.BtnUpdateEmployee = ((System.Windows.Controls.Button)(target));
            return;
            case 43:
            this.BtnDeleteEmployee = ((System.Windows.Controls.Button)(target));
            return;
            case 44:
            this.BtnAddEmployee = ((System.Windows.Controls.Button)(target));
            return;
            case 45:
            this.BtnAddEmployeeToDepartment = ((System.Windows.Controls.Button)(target));
            
            #line 251 "..\..\..\Views\MainWindow.xaml"
            this.BtnAddEmployeeToDepartment.Click += new System.Windows.RoutedEventHandler(this.BtnAddEmployeeToDepartment_OnClick);
            
            #line default
            #line hidden
            return;
            case 46:
            this.LblEmployeeid = ((System.Windows.Controls.Label)(target));
            return;
            case 47:
            this.EmployeeGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

