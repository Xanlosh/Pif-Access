﻿using System.Collections.Generic;
using System.Linq;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;
using DBFormsDapper.Models;
using iTextSharp.text;

namespace DBFormsDapper.Controllers
{
    public class DepartmentController
    {
        public List<Department> DepartmentList;
        public DepartmentController()
        {
            DepartmentList = Connection.DbConnection.GetAll<Department>().ToList();
        }
    }
}