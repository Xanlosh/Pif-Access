﻿using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;
using DBFormsDapper.Models;

namespace DBFormsDapper.Controllers
{
    public class EmployeeController
    {
        public List<Employee> EmployeeList;
        
        public EmployeeController()
        {
            Refresh();
        }

        public void Refresh()
        {
            EmployeeList = (List<Employee>) Connection.DbConnection.GetAll<Employee>();
        }
    }
}