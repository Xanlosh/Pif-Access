﻿using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;
using DBFormsDapper.Models;

namespace DBFormsDapper.Controllers
{
    public class AddressController
    {
        public List<Address> AdressList;
        
        public AddressController()
        {
            Refresh();
        }

        public void Refresh()
        {
            AdressList = (List<Address>) Connection.DbConnection.GetAll<Address>();
        }
    }
}