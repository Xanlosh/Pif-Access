﻿using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;
using DBFormsDapper.Models;

namespace DBFormsDapper.Controllers
{
    public class ClientController
    {
        public List<Client> ClientList;
        
        public ClientController()
        {
            Refresh();
        }

        public void Refresh()
        {
            ClientList = (List<Client>) Connection.DbConnection.GetAll<Client>();
        }
        
    }
}