﻿using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;
using DBFormsDapper.Models;

namespace DBFormsDapper.Controllers
{
    public class CountryController
    {
        public List<Country> CountryList;
        
        public CountryController()
        {
            Refresh();
        }

        public void Refresh()
        {
            CountryList = (List<Country>) Connection.DbConnection.GetAll<Country>();
        }
    }
}