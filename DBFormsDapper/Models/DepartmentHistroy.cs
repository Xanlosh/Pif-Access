﻿using System;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

namespace DBFormsDapper.Models
{
    [Table("tblWork")]
    public class DepartmentHistroy : Model
    {
        [ExplicitKey]
        public int fiEmployee { get; set; }
        public int fiDepartment { get; set; }
        public DateTime dtJoinDate { get; set; }

        public Employee GetEmployee()
        {
            return Connection.DbConnection.Get<Employee>(fiEmployee);
        }

        public Department GetDepartment()
        {
            return Connection.DbConnection.Get<Department>(fiDepartment);
        }
    }
}