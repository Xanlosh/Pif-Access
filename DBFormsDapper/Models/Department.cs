﻿using Dapper.Contrib.Extensions;
// ReSharper disable InconsistentNaming

namespace DBFormsDapper.Models
{
    [Table("tblDepartment")]
    public class Department
    {
        [Key]
        public int idDepartment { get; set; }
        public string dtTitle { get; set; }
        public string dtEmail { get; set; }
        public string dtPhonenumber { get; set; }
        public int fiLeader { get; set; }
    }
}