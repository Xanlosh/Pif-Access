﻿using System;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

// ReSharper disable InconsistentNaming

namespace DBFormsDapper.Models
{
    [Table("tblVacation")]
    public class VacationRequest
    {
        [Key]
        public int idVacation { get; set; }
        public DateTime dtRequestDate { get; set; }
        public DateTime dtStartDate { get; set; }
        public DateTime dtEndDate { get; set; }
        public bool dtIsApproved { get; set; }
        public DateTime dtRevisingDate { get; set; }
        public string dtComment { get; set; }
        public int fiClaimant { get ; set; }

        public Employee getClaimant ()
        {
            return Connection.DbConnection.Get<Employee>(fiClaimant);
        }
    }
}