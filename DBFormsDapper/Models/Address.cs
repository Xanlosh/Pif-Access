﻿using System;
using System.Windows;
using Dapper;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

namespace DBFormsDapper.Models
{
    [Table("tblAddress")]
    public class Address : Model
    {
        // ReSharper disable InconsistentNaming
        [Key]
        public int idAddress { get; set; }
        public string dtCity { get; set; }
        public string dtZipcode { get; set; }
        public string dtStreetname { get; set; }
        public string dtStreetnumber { get; set; }
        public bool? dtisBillingaddress { get; set; }
        public int fiCountry { get; set; }
        public int fiClient { get; set; }


        public Client GetClient()
        {
            return Connection.DbConnection.Get<Client>(fiClient);
        }

        public Country GetCountry()
        {
            return Connection.DbConnection.Get<Country>(fiCountry);
        }
        
        public void Add()
        {
            int result = Connection.DbConnection.Execute(
                "Insert into tblAddress " +
                "(dtCity,dtZipcode,dtStreetname,dtStreetnumber,dtisBillingaddress,fiCountry,fiClient) " +
                "VALUES (@parm1,@parm2,@parm3,@parm4,@parm5,@parm6,@parm7);",
                new
                {
                    parm1 = dtCity,
                    parm2 = dtZipcode,
                    parm3 = dtStreetname,
                    parm4 = dtStreetnumber,
                    parm5 = dtisBillingaddress,
                    parm6 = fiCountry,
                    parm7 = fiClient
                }
            );
            MessageBox.Show(result > 0 ? "Query executed successfully" : "Oops something went wrong!");
        }

        public void Delete()
        {
            Connection.DbConnection.Delete(this);
            MessageBox.Show("Deleted successfully.");
        }

        public void Update()
        {
            int result = Connection.DbConnection.Execute(
                "UPDATE tblAddress SET dtCity = @parm1,dtZipcode = @parm2,dtStreetname = @parm3,dtStreetnumber = @parm4,dtisBillingaddress = @parm5,fiCountry = @parm6,fiClient = @parm7 WHERE idAddress = @parm8",
                new
                {
                    parm1 = dtCity,
                    parm2 = dtZipcode,
                    parm3 = dtStreetname,
                    parm4 = dtStreetnumber,
                    parm5 = dtisBillingaddress,
                    parm6 = fiCountry,
                    parm7 = fiClient,
                    parm8 = idAddress
                }
            );
        }
    }
}