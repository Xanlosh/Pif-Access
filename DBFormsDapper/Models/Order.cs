﻿using System;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

namespace DBFormsDapper.Models
{
    [Table("tblOrder")]
    public class Order
    {
        [Key]
        public int idOrder { get; set; }
        public DateTime dtOrderDate { get; set; }
        public bool dtisPaid { get; set; }
        public int fiClient { get; set; }
        public int fiAddress { get; set; }

        public Address getAddress()
        {
            return Connection.DbConnection.Get<Address>(fiAddress);
        }

        public Client getClient()
        {
            return Connection.DbConnection.Get<Client>(fiClient);
        }
        
    }
}