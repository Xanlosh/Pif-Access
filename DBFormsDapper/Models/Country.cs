﻿using Dapper.Contrib.Extensions;

namespace DBFormsDapper.Models
{
    [Table("tblCountry")]
    public class Country
    {
        [Key]
        // ReSharper disable InconsistentNaming
        public int idCountry { get; set; }
        public string dtCountryName { get; set; }
        public string dtContinent { get; set; }
        public string dtCapital { get; set; }
        public string dtPhoneprefix { get; set; }
        public string dtDomain { get; set; }
    }
}