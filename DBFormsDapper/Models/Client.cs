﻿using System;
using System.Windows;
using Dapper;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

namespace DBFormsDapper.Models
{
    [Table("tblClient")]
    public class Client : Model
    {
        // ReSharper disable InconsistentNaming
        [Key]
        public int idClient { get; set; }      
        public string dtFirstname { get; set; }
        public string dtLastname { get; set; }
        public DateTime dtBirthdate { get; set; }
        public string dtUsername { get; set; }
        public string dtEmail { get; set; }
        public DateTime dtRegistrationdate { get; set; }
        public string dtPassword { get; set; }
        public double dtPricescaling { get; set; }

        public string FullName()
        {
            return dtFirstname + " " + dtLastname;
        }

        public void Update()
        {
            //Update function rewritten because Oledb does not support named parameters but takes them in order no matter what.
            int result = Connection.DbConnection.Execute(
                "Update tblClient SET dtFirstname = @parm1 ,dtLastname = @parm2 ,dtBirthdate = @parm3 ,dtUsername = @parm4 ,dtEmail = @parm5 ,dtRegistrationdate = @parm6 ,dtPassword = @parm7 ,dtPricescaling = @parm8" +
                " WHERE idClient = @parm9",
                new
                {
                    parm1 = dtFirstname,
                    parm2 = dtLastname,
                    parm3 = dtBirthdate,
                    parm4 = dtUsername,
                    parm5 = dtEmail,
                    parm6 = dtRegistrationdate,
                    parm7 = dtPassword,
                    parm8 = dtPricescaling,
                    parm9 = idClient
                }
            );
            MessageBox.Show(result > 0 ? "Query executed successfully!" : "Oops something went wrong.");
        }
        
        public void Add()
        {
            int result = Connection.DbConnection.Execute(
                "Insert Into tblClient (dtFirstname,dtLastname,dtBirthdate,dtUsername,dtEmail,dtRegistrationdate,dtPassword,dtPricescaling) Values (@parm1,@parm2,@parm3,@parm4,@parm5,@parm6,@parm7,@parm8)",
                new
                {
                    parm1 = dtFirstname,
                    parm2 = dtLastname,
                    parm3 = dtBirthdate,
                    parm4 = dtUsername,
                    parm5 = dtEmail,
                    parm6 = dtRegistrationdate,
                    parm7 = dtPassword,
                    parm8 = dtPricescaling,
                }
            );
            MessageBox.Show(result > 0 ? "Client added successfully!" : "Oops something went wrong.");
        }
        
        public void Delete()
        {
            Connection.DbConnection.Delete(this);
            MessageBox.Show("Deleted successfully.");
        }
    }
}