﻿using System;
using Dapper.Contrib.Extensions;

namespace DBFormsDapper.Models
{
    [Table("tblAdd")]
    public class OrderContains
    {
        [ExplicitKey]
        public int fiProduct { get; set; }
        [ExplicitKey]
        public int fiOrder { get; set; }
        public int dtAmount { get; set; }
        public DateTime dtShipDate { get; set; }
    }
}