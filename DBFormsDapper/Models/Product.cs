﻿using Dapper.Contrib.Extensions;

namespace DBFormsDapper.Models
{
    [Table("tblProduct")]
    public class Product
    {
        [Key]
        public int idProduct { get; set; }
        public string dtProductNumber { get; set; }
        public string dtProductTitle { get; set; }
        public string dtDescription { get; set; }
        public string dtProductionPrice { get; set; }
        public string dtSellPrice { get; set; }
    }
}