﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using DBFormsDapper.Core;

namespace DBFormsDapper.Models
 {
     [Table("tblEmployee")]
     public class Employee : Model
     {
         // ReSharper disable InconsistentNaming
         [Key] 
         public int idEmployee { get; set; }
         public string dtFirstname { get; set; }
         public string dtLastname { get; set; }
         public DateTime dtBirthdate { get; set; }
         public string dtMatriculation { get; set; }
         public string dtPhonenumber { get; set; }
         public string dtPrivatephonenumber { get; set; }
         public string dtPrivateemail { get; set; }
         public string dtEmail { get; set; }
         public string dtCity { get; set; }
         public string dtZipcode { get; set; }
         public string dtStreetname { get; set; }
         public string dtStreetnumber { get; set; }
         public int fiCountry { get; set; }       
         private string dtTitle { get; set; }

         public string FullName()
         {
             return dtFirstname + dtLastname;
         }
         
         public string getDepartment()
         {
             if (dtTitle != null)
             {
                 return dtTitle;
             }

             var department = Connection.DbConnection
                 .Query(
                     "SELECT dtTitle From tblDepartment INNER JOIN tblWork ON tblDepartment.idDepartment = tblWork.fiDepartment Where fiEmployee = "+ idEmployee +" Order By dtJoinDate DESC")
                 .First();
             return department.dtTitle;
         }

         public List<dynamic> GetDepartmentHistory()
         {
             List<object> Departments = new List<object>();
             var results = Connection.DbConnection.Query("SELECT * FROM tblDepartment INNER JOIN tblWork On tblDepartment.idDepartment = tblWork.fiDepartment WHERE fiEmployee = "+ idEmployee +" Order By dtJoinDate DESC;").ToList();
             foreach (var result in results)
             {
                 foreach (var item in result)
                 {
                     Departments.Add(new
                     {
                         JoinDate = item.dtJoinDate,
                         Title = item.dtTile                        
                     });
                 }
             }

             return Departments;
         }
     }
 }